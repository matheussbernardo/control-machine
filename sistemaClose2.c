#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <zmq.h>

#define esteira 7
#define sensorFluxo 21
#define sensorNivel 3
#define emergencia 1
#define liga 27
#define bomba 29
#define sensorEncher 22
#define sensorFechar 5

#define pistao 0
#define sensorIndutivo 24
#define sensorPosicaoTampa 4

// Flags

int ehMesmaGarrafa1 = 0;
int ehMesmaGarrafa2 = 0;
int ehMesmaGarrafa3 = 0;

// Interacoes com sensores

int botaoLigadoPressionado(){
	return digitalRead(liga);
}

int botaoEmergenciaPressionado(){
	return digitalRead(1);
}

void ligaEsteira(){
	digitalWrite(esteira, HIGH);
}

void desligaEsteira(){
	digitalWrite(esteira, LOW);
}

int verificaSensorPosicao1(){
	return digitalRead(sensorEncher);
}

int verificaSensorPosicao2(){
	return digitalRead(sensorPosicaoTampa);
}

int verificaSensorPosicao3(){
	return digitalRead(sensorFechar);
}

int garrafaComTampa(){
	return !digitalRead(sensorIndutivo);
}

// Funcoes de acao

void envasar(){
	if(verificaSensorPosicao1() && !ehMesmaGarrafa1){
		ehMesmaGarrafa1 = 1;
		desligaEsteira();
		printf("Enchendo...");
		sleep(8);
	}else{
		ehMesmaGarrafa1 = 0;
	}
}

void fechaGarrafa(){
	if(verificaSensorPosicao3() && !ehMesmaGarrafa3){
		ehMesmaGarrafa3 = 1;
		desligaEsteira();
		printf("Fechando...");
		sleep(12);
	}else{
		ehMesmaGarrafa3 = 0;
	}
}

int main(){
	while(1){
		if(botaoLigadoPressionado() && !botaoEmergenciaPressionado()){
			printf("Funcionando...");
			envasa();
			fechaGarrafa();
			ligaEsteira();
		}else if(botaoEmergenciaPressionado()){
			printf("Botao de emergencia pressionado");
			desligaEsteira();
		}else{
			printf("Nem ta ligado");
		}
	}
	return 0;
}
